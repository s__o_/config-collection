# Config Collection

This repository contains a collection of configuration files for frequently used application on Linux or macOS. This make my life easier for setting up new machines.

## Config Overview

1. zsh/ohmyzsh
2. i3 window manager
3. Tilix
