# Import/Export Tilix Configurations

## Export 
`dconf dump /com/gexperts/Tilix/ > tilix.dconf`

## Import 
`dconf load /com/gexperts/Tilix/ < tilix.dconf`

